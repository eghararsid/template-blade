<!DOCTYPE html>
<html>
	<header>
		<!-- Nama Pada Bagian Tab -->
		<title> Buat Akun </title>

		<!-- Character Set UTF-8 covers almost all characters in the world -->
		<meta charset="utf-8">

		<!-- Make Wep Page look good on all devices -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</header>

	<body>

		<div>
			<h1> Buat Account Baru! </h1>
			<h3> Sign Up Form </h3>
		</div>

		<!-- Membuat Form dan Arahkan ke Welcome Page -->
		<form action="/welcome" method="POST">
			@csrf
			<!-- Menggunakan Text Field Untuk Input Nama -->
			<p> First name: </p>
			<input type="text" id="nama_awal" name="fname">

			<p> Last name: </p>
			<input type="text" id="nama_akhir" name="lname">

			<!-- Membuat Pilihan Gender Menggunakan Radio -->
			<P> Gender: </P>
			<input type="radio" name="gender"> Male <br>
			<input type="radio" name="gender"> Female <br>
			<input type="radio" name="gender"> Other <br>

			<!-- Membuat Pilihan Kewarganegaraan Menggunakan Select Option -->
			<p> Nationality: </p>
			<select>
				<option value="Indo"> Indonesian </option>
				<option value="Sng"> Singaporean</option>
				<option value="Malay"> Malaysian </option>
				<option value="Aus"> Australian </option>
			</select>

			<!-- Pilihan Lebih Sari Satu Menggunakan Checkbox -->
			<p> Language Spoken: </p>
			<input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
			<input type="checkbox" name="bahasa"> English <br>
			<input type="checkbox" name="bahasa"> Other <br>

			<!-- Memasukkan Biodata Menggunakan Text Field Dengan Ukuran Bisa Disesuaikan -->
			<p> Bio: </p>
			<textarea cols="35" rows="10" id="biodata"></textarea> <br>

			<!-- Menyelesaikan Form dan Pergi Ke 'Form Action' -->
			<input type="submit" value="Sign Up">

		</form>

	</body>
</html>