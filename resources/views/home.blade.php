<!DOCTYPE html>
<html>
	<header>
		<!-- Nama Pada Bagian Tab -->
		<title> Sanber Book </title>

		<!-- Character Set UTF-8 covers almost all characters in the world -->
		<meta charset="utf-8">

		<!-- Make Wep Page look good on all devices -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</header>

	<body>

		<!-- Judul Tag Header -->
		<div>
			<h1> SanberBook </h1>
		</div>

		<!-- Introduction Tag Paragraf -->
		<div>
			<h2> Social Media Developer Santai Berkualitas </h2>
			<p> Belajar dan Berbagi agar hidup ini semakin santai berkualitas </p>
		</div>

		<!-- Pengenalan Manfaat SanberBook Menggunakan Tag List <ul>  -->
		<div>
			<h3> Benefit Join di SanberBook </h3>
			<ul>
				<li> Mendapatkan motivasi dari sesama developer </li>
				<li> Sharing knowledge dari para mastah Sanber </li>
				<li> Dibuat oleh calon web developer terbaik </li>
			</ul>
		</div>

		<!-- Join SanberBook Melalui Tag List <ol> dan Tag Link -->
		<div>
			<h3> Cara Bergabung ke SanberBook </h3>
			<ol>
				<li> Mengunjungi Website ini </li>
				<li> Mendaftar di <a href="/register">Form Sign Up</a> </li>
				<li> Selesai! </li>
			</ol>
		</div>

	</body>
</html>